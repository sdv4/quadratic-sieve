import java.util.*;
import java.lang.*;
import java.io.*;
import java.math.BigInteger;
import java.math.BigDecimal; 
/* 
 *
 * Based on code found at: https://stackoverflow.com/questions/9767321/how-to-generate-all-possible-combinations-n-bit-strings
 */
public class BinaryPermutations {
 

	/** Method to determine all possible permutations of 0 and 1 for a specified length - 1,
	 *  from a given alphabet. Made for KSIQS class.
	 *
	 *  @param n	length of permutations
	 *  @return	array of booleans, with 0 representing -1 and 1 representing +1, first element is always 1
	 *  
	 */
	public static ArrayList<int[]> permutation(int n) {
    		ArrayList<int[]> permutations = new ArrayList<int[]>();
		BigInteger binary = BigInteger.ZERO;
    		BigDecimal rows = new BigDecimal(Math.pow(2, n - 1));
		int[] perms;
    		while (binary.compareTo(rows.toBigInteger()) < 0) {
        		String bin = binary.toString(2);			
        		while (bin.length() < n)
            			bin = "0" + bin;
        		char[] chars = bin.toCharArray();
        		boolean[] boolArray = new boolean[n];
        		for (int j = 0; j < chars.length; j++) {
        		    boolArray[j] = chars[j] == '0' ? true : false;
        		}
			perms = new int[n];
			for(int i = 0; i < boolArray.length; i++){
				if(boolArray[i] == true)
					perms[i] = 1;
				else
					perms[i] = -1;

			}			
			permutations.add(perms);
			binary = binary.add(BigInteger.ONE);
    		}
		return permutations;
	}
	// One test case for permutation method.
	public static void main (String[] args)
	{
		ArrayList<int[]> allSigns = permutation(5);
		for(int[] c : allSigns)
		{
			for(int elmnt : c)
				System.out.print(elmnt);
			System.out.println();		
		}

	}

}// end class
