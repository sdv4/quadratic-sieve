import java.math.BigInteger;

/**
* Iterative method for calculating the ceiling of the square root of a number.
* Adapted from the code found at:
* https://stackoverflow.com/questions/4407839/how-can-i-find-the-square-root-of-a-java-biginteger
*
* @version	23 July 2017
*
*/

public class BigIntegerSqrt {

	public static BigInteger bigIntegerSqrtCeiling(BigInteger radicand) 
       		throws IllegalArgumentException	{ 
		if(radicand.compareTo(BigInteger.ZERO) < 0) {
			throw new IllegalArgumentException("Negative Argument.");
		}

		// Handle trivial cases
		if(radicand == BigInteger.ZERO || radicand == BigInteger.ONE)
			return radicand;


		BigInteger two = BigInteger.valueOf(2L);
		BigInteger y;
		for(y = radicand.divide(two); y.compareTo(radicand.divide(y)) > 0; y = ((radicand.divide(y)).add(y)).divide(two)){}

		if(radicand.compareTo(y.multiply(y)) == 0)
			return y;
		else
			return y.add(BigInteger.ONE);
		

	} // end bigIntegerSqrtCeiling


} // end class
