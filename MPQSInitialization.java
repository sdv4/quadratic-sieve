import java.lang.Math.*;
import java.util.*;
import java.math.BigInteger;
public class MPQSInitialization {

	public static void getSmoothRelations(int N, int F){
	
		int[][] S;									// Array that will hold the smooth relations when returned.
		ArrayList<Integer> factorBase;							// Primes which will be used in the sieving stage. 
		int[] modSqrts;									// Will hold t in t^2 \equiv N (mod p) for each p in Factor Base
		int[] solution1;								// Will hold a^-1(t - b) \equiv N (mod p) for each prime in factor base
		int[] solution2;								// Will hold a^-1(-t - b) \equiv N (mod p) for each prime in factor base
		int[] logp;									// Approximate logarithm for each prime in the factor base
		int K;										// Method will return when K+1 smooth relations have been found.
		int offset = 0;									// Used if/when sieving stage returned to
		int a;										// Term 'a' in g_a,b(x) = (ax+b)^2 - N 
		int M;										// Term in bound [-M,M]
		int b;										// Term 'b' in g_a,b(x)
	//
	//check preconditions of N are met here
	//


	/* Compute Startup Data */
		M = 10;		
		double squareRootN = Math.sqrt(N);		// can delete???
		b = (int) Math.ceil(squareRootN);						// Compute the constant term \ceil(\sqrt(N)). Used in the Sieving Stage
		factorBase = determineFactorBase(F, N);						// Initialize factorBase.
		K = factorBase.size();
		S = new int[K+1][2];								// Elements are pairs (x,(x+b)^2-N)				
		logp = new int[K];
		modSqrts = new int[K];								// Initialize modSqrts to have space for each element of factorBase
		modSqrts[0] = 1;								// TonelliSqrtModP requires odd prime, so only even prime hard coded
		solution1 = new int[K];
		solution2 = new int[K];

		for(int i = 1; i < modSqrts.length; i++) {					// Solve t in t^2 \equiv N (mod p) for each p in factorBase
			modSqrts[i] = TonelliSqrtModP.SqrtModP(N, factorBase.get(i));
		}
		for(int i = 0; i < logp.length; i++) {
			logp[i] = (int) Math.round(Math.log(factorBase.get(i))/Math.log(2));	// Get approximate log_2(p) using change of base for each factor base prime
		}





	/* Initialization Stage */

		// Find q and set a
		int q = (int) Math.round(Math.sqrt(Math.sqrt(2 * N)/M));			// q approx sqrt(sqrt(2N)/M)
		while(!factorBase.contains(q))							// Get F-smooth a with Jacobi (q/N)=1 
			q++;
		a = q * q;									// Set a = q^2
//	System.out.println("Value of 'a' is: " + a);	
//	System.out.println("Value of 'q' is: " + q);
		// Compute b
		int NmodA = N % a;								// Reduce N (mod a)		
    		int bPrime = TonelliSqrtModP.SqrtModP(N, q);					// Get b'=b in b^2 \equiv N (mod q)
//	System.out.println("Value of N (mod a) is: " + NmodA);
//	System.out.println("Value of b': " + bPrime);
		while(((bPrime * bPrime) % a) != NmodA)
			bPrime += q;
		b = bPrime;
//		System.out.println("Calculated value of 'b' is: " + b);

		// Compute soln1_p and soln2_p for each p in factor base

		for(int i = 0; i < solution1.length; i++){
			if(q != factorBase.get(i)){
			BigInteger biga = new BigInteger(Integer.toString(a));
			BigInteger bigp = new BigInteger(Integer.toString(factorBase.get(i)));
//	System.out.println("biga is: " + biga + ". bigp is: " + bigp);
			BigInteger bigaModInv = biga.modInverse(bigp);
			int aInverseModp = bigaModInv.intValue();
			solution1[i] = Math.floorMod((aInverseModp*(modSqrts[i] - b)), factorBase.get(i)); 
			solution2[i] = Math.floorMod(((aInverseModp*(-1*modSqrts[i])) - b), factorBase.get(i));
			}
			else{
				solution1[i] = -1;
				solution2[i] = -1;		// just marking unusable solutions.
			}
		}
		System.out.println("soln1_p = ");
		for(int i = 0; i < solution1.length; i++)
			System.out.println(solution1[i]);
		System.out.println("soln2_p = ");
		for(int i = 0; i < solution2.length; i++)
			System.out.println(solution2[i]);


	}

	/* Determine the factor base: the
	 * smooth primes such that N is a 
	 * quadratic residue (mod p). I.e. Legendre symbol
	 * (N/p) = 1
	 *
	 * @param F  the smoothness bound
	 * @param N  the composite begin factored
	 * @return  a list containing the elements of the 
	 * factor base. 
	 */
	private static ArrayList<Integer> determineFactorBase(int F, int N) {
		// First get primes <= F into an array
		int[] primes = SieveOfEratosthenes.basicEratos(F);  
		ArrayList<Integer> factorBase = new ArrayList<Integer>();
		// Next determine which ones give leg (N/p)=1
		// by calling DetermineQuadRes
		for(int i = 0; i < primes.length; i++) {
			int p = primes[i];
			if(DetermineQuadRes.LegJacSym(N, p) == 1)
				factorBase.add(p);
		}
		

		return factorBase;
	}


	public static void main(String[] args){
		getSmoothRelations(62113, 37);
	}
	
	
}

