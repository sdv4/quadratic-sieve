import java.lang.Math.*;
import java.util.*;
import java.math.BigInteger;

/** This is an implementation of the Sieving portion of the Self Initializing 
* Quadratic Sieve factoring algorithm. Built on code written in authours
* MPQS implementation and based on the pseduocode provided in 
* the thesis 'Factoring Integers with the Self-Initializing Quadratic Sieve' by
* Scott P. Contini. The notation matches
* that found in this thesis as far as possible so as to increase the didactic 
* value of this class. 
*
* @author Shane Sims <shane.sims.ss@gmail.com>
* @version 5 July 2017 
*
*/

public class SIQS {	

	/**
	* This method determine the smooth relations required to factor N.
	* 
	* @param N	The integer to be factored. Must be an odd composite
	* free of prime powers.   
	* @param F	The smoothness bound to which the returned relations will
	* be bound by.
	* @return 	An array with each element containing an array with length 2, 
	* representing an ordered pair of the form (x, (ax+b)^2 - N), where the value 
	* x leads (ax + b)^2-n to be B-smooth.
	*
	*/
	public static int[][] getSmoothRelations(int N, int F){
	
		int[][] S;									// Array that will hold the smooth relations when returned.
		ArrayList<Integer> factorBase;							// Primes which will be used in the sieving stage. 
		int[] modSqrts;									// Will hold t in t^2 \equiv N (mod p) for each p in Factor Base
		int[] solution1;								// Will hold a^-1(t - b) \equiv N (mod p) for each prime in factor base
		int[] solution2;								// Will hold a^-1(-t - b) \equiv N (mod p) for each prime in factor base
		int[] logp;									// Approximate logarithm for each prime in the factor base
		int K;										// Method will return when K+1 smooth relations have been found. Size of factorbase
		int a;										// Term 'a' in g_a,b(x) = (ax+b)^2 - N 
		int M;										// Term in bound [-M,M]
		int b;										// Term 'b' in g_a,b(x)
		int polys;									// The number of polynomials sieved.
		ArrayList<Integer> aFactors;							// The prime factors, the product of which forms a
		int[] aInversep;								// will hold the inverse of a mod p_i where p_i is a prime in the factor base. Or -1 if p_i|a
	//
	//check preconditions of N are met here
	//


	/* Compute Startup Data */
		polys = 0;
		M = F/2;									// Smaller M than MPQS TODO: experiment with size of M
		double squareRootN = Math.sqrt(N);		
		factorBase = determineFactorBase(F, N);						// Initialize factorBase.
		K = factorBase.size();
		S = new int[K+1][4];								// Elements are pairs (x,(x+b)^2-N)				
		logp = new int[K];
		modSqrts = new int[K];								// Initialize modSqrts to have space for each element of factorBase
		modSqrts[0] = 1;								// TonelliSqrtModP requires odd prime, so only even prime hard coded
		solution1 = new int[K];
		solution2 = new int[K];
		b = 0;
		for(int i = 1; i < modSqrts.length; i++) {					// Solve t in t^2 \equiv N (mod p) for each p in factorBase
			modSqrts[i] = TonelliSqrtModP.SqrtModP(N, factorBase.get(i));
		}
		for(int i = 0; i < logp.length; i++) {
			logp[i] = (int) Math.round(Math.log(factorBase.get(i))/Math.log(2));	// Get approximate log_2(p) using change of base for each factor base prime
		}
System.out.println("Factor base:");

for(int i = 0; i < factorBase.size(); i++){
	System.out.println(factorBase.get(i));
//	System.out.println("tmem_" + factorBase.get(i) + " : " + modSqrts[i]);
}

		//NOTE: this version will break when the required number of smooth relations has been found or will return error message if 
		//K+1 relations could not be found given a single a. 
	
	/* Initialization Stage */
		int smoothRelFound = 0;								// Tracks the number of smooth relations verified by trial division. 
		aFactors = new ArrayList<Integer>();
		// Determine a as factor of FB primes approx sqrt(2N)/M
		// TODO: this version just gets three primes to equal a. This is based on the observation
		// that N = 101868649 required 2 polynomials in MPQS. So such an a will generate 2^2 polynomials which will
		// hopefully be enough for the decreased size of M in SIQS
		//TODO: implement loop to choose a when all b's generated have been used up. 
		//TODO: this method of finding a is just to get SIQS working. Explore better ways for finding a.


		int fBaseCutoffIndex = (int) Math.ceil(factorBase.size() * .06);

		int aTarget = (int) Math.round(Math.sqrt(2.0 * N)/M);
		int testA = factorBase.get(fBaseCutoffIndex) * factorBase.get(fBaseCutoffIndex + 1) * factorBase.get(fBaseCutoffIndex + 2); // Get smallest three primes above cutoff
		a = testA;
System.out.println("testA: " + testA);
		
		// This case may happen if N is too small - and certainly will happen when N is an int value. Only gets two factors for a and will require switching A some number of times
		if(testA > aTarget){
			int tempA = aTarget/factorBase.get(fBaseCutoffIndex);
System.out.println("tempA: " + tempA);

			aFactors.add(factorBase.get(fBaseCutoffIndex));
			int smallTempA = tempA-1;						// Ensures a isn't a square of a single prime
			int bigTempA = tempA+1;
	
			while(!factorBase.contains(smallTempA)){
				smallTempA--;
		
			}
			
			while(!factorBase.contains(bigTempA))
				bigTempA++;

			if(smallTempA == factorBase.get(fBaseCutoffIndex))
				aFactors.add(bigTempA);

			else if(tempA - smallTempA > bigTempA - tempA)
				aFactors.add(bigTempA);
			else
				aFactors.add(smallTempA);
			a = aFactors.get(0) * aFactors.get(1);					// a is product of two FB primes
		}
		else {
			int i = 3;								// index of next FB prime to add to A
			while(testA < aTarget){
				a = testA;							// save in a the previous value less than the target
				testA *= factorBase.get(i);					// add another prime factor to testA
				i++;
			}									// when loop breaks, a is as large as can be without going over targetA

		}



System.out.println("a: " + a);


		// Initialize Polynomials here
		int[] B = new int[aFactors.size()];
		int[][] Bainv2jp = new int[factorBase.size()][aFactors.size()];			// To be used in initializing all but first polynomial
		aInversep = new int[factorBase.size()];
		int numOfPossiblePolys = (int) Math.pow(2, (aFactors.size() - 1));			// Number of polynomials that can be generated from a

		//Init polys
		while(polys < numOfPossiblePolys){
		
			if(polys == 0){								// Initialize first polynomial
				for(int i = 0; i < aFactors.size(); i++){
					int p =	aFactors.get(i);				// The i'th prime factor of a
System.out.println("p: " + p);

					BigInteger bigAOverP = new BigInteger(Integer.toString(a/p));
					BigInteger bigp = new BigInteger(Integer.toString(p));
					BigInteger bigModInv = bigAOverP.modInverse(bigp);
					int invModP = bigModInv.intValue();
System.out.println("should be 6: " + invModP);
					int delta = (modSqrts[factorBase.indexOf(p)] * invModP) % p;
//System.out.println("delta: " + delta);
					if(delta > (p/2.0))
						delta = p - delta;
System.out.println("p/2 " + (p/2.0));
					
					B[i] = delta * (a/p);
System.out.println("B[" + i + "]: " + B[i]);
				}// end first for
				for(int i = 0; i < factorBase.size(); i++){
					int p = factorBase.get(i);
					if(!aFactors.contains(p)){
						BigInteger biga = new BigInteger(Integer.toString(a));
						BigInteger bigp = new BigInteger(Integer.toString(p));
						BigInteger bigInvAmodp = biga.modInverse(bigp);
						aInversep[i] = bigInvAmodp.intValue();
					}
					else
						aInversep[i] = -1;				
				 	for(int j = 0; j < aFactors.size(); j++){
						Bainv2jp[i][j] = (2 * B[j] * aInversep[factorBase.indexOf(p)]) % p;
					}
				}//end second for
				for(int i = 0; i < aFactors.size(); i++)
					b += B[i];						// initialize b_1
System.out.println("b: " + b);

				// Compute solution1 and solution2 for this polynomial

				for(int i = 0; i < factorBase.size(); i++){
					int p = factorBase.get(i);
					if(!aFactors.contains(p)){				// make sure p does not divide a
						solution1[i] = Math.floorMod((aInversep[i]*(modSqrts[i] - b)), p); 
						solution2[i] = Math.floorMod(((aInversep[i]*(-1*modSqrts[i])) - b), p);
					}
					else{
						solution1[i] = -1;
						solution2[i] = -1;
					}
				}
				

			}// end Init first Poly
			else{									// Initialize subsequent polynomials
				int v = polys & (-1 * polys);					// Bitwise AND to get position of right most 1 in bin rep of polys sieved
				// Switch b
				int greyCodeExp = (int) Math.ceil(polys / (Math.pow(2, v)));
				b = b + (int) (2 * Math.pow(-1, greyCodeExp) * B[v]);			
				// Compute solutionq and solution2 for this polynomial
				for(int k = 0; k < factorBase.size(); k++){
					int p = factorBase.get(k);
					if(!aFactors.contains(p)){				// make sure p does not divide a
						solution1[k] += Math.floorMod((int) (Math.pow(-1, greyCodeExp)*Bainv2jp[k][v]), p);
						solution2[k] += Math.floorMod((int) (Math.pow(-1, greyCodeExp)*Bainv2jp[k][v]), p);
					}
					else{
						solution1[k] = -1;
						solution2[k] = -1;
					}

				}
			}// end init subsequent polys	
		
	/* Sieving Stage */
	
		int index1 = 0;									// Will hold latest value of solution1_p + ip
		int index2 = 0;									// Will hold latest value of solution2_p + ip
		int[] ithMultipleP = new int[K];						// Will hold i'th multiple of p sieved with so fa
		
		
		System.out.println("Smooth relations found: " + smoothRelFound);
		System.out.println("Size of factor base: " + K);
		int[] sieveArray = new int[2*M + 1];							// Initialize sieve array with 0's.
		for(int j = 0; j < factorBase.size(); j++) {					// For each prime in the factor base.
		int k = 0;	
			while(true){				
				int p = factorBase.get(j);
	System.out.println("P: " + p);
				int s1 = solution1[j];
				int s2 = solution2[j];
				if(s1 != -1){
					int i = -1;
				//find lower bound value for i such that soln1 +i*p >= -M
					while(s1 + i*p >= -M && s1 + i*p <= M){
						index1 = s1 + i*p;
	System.out.println("solution1: " + s1);
	System.out.println("index1: " + index1);
						
	
						sieveArray[index1 + M] += logp[j];		// add M to offset sieve array starts at -M = sieveArray[0]
						i--;
					}
					i = 0;
					while(s1 + i*p <= M){						
						index1 = s1 + i*p;
	System.out.println("index1: " + index1);


						sieveArray[index1 + M] += logp[j];
						i++;
					}
					if(p != 2){						// If p = 2, sieve only with solution1
						i = -1;
						while(s2 + i*p >= -M && s2 + i*p <= M){
							index1 = s2 + i*p;
	System.out.println("index1*: " + index1);
	
							sieveArray[index1 + M] += logp[j];
							i--;
						}
						i = 0;
						while(s2 + i*p <= M){
							index1 = s2 + i*p;

	System.out.println("index1**: " + index1);
	
							sieveArray[index1 + M] += logp[j];
							i++;
						}
					}
				}//end if
				break;
			}//end while
		}//end for
	

	/* Trial Division Stage */

		int possiblySmooth = 0;
		int[][] candidates = new int[M][2];
		for(int x = 0; x < sieveArray.length; x++){						// Scan sieve array for locations x indicating potential g_a,b(x) smooth

			double testTerm = Math.log(M*(squareRootN))/Math.log(2);			// Test condition for values at location x: log_2(2*x*\sqrt(N))
			System.out.println("Sieve array index " + x + ": " + sieveArray[x] + "    testTerm:" + (testTerm-4.5));

			if(sieveArray[x] >= (testTerm-4.5)){						//TODO Needs experimentation to optimize
				
				candidates[possiblySmooth][0] = x - M;					// Mark x as possibly having g_a,b(x) as F-Smooth
	System.out.println("Possibly smooth: x = " + candidates[possiblySmooth][0]);
				candidates[possiblySmooth][1] = (a*(x-M) + b)*(a*(x-M) + b) - N;
				possiblySmooth++;					
			}
		}

		for(int i = 0; i < possiblySmooth; i++){						// For each potentially smooth g(x)
			int checkMe = candidates[i][1];							// Number to trial divide for smoothness
			if(checkMe < 0){								// Factor out -1 for trial division
				checkMe *= -1;								// Make positive for trial division
			}
			for(int j = 0; j < factorBase.size(); j++){					// For each prime in the factor base
				int p = factorBase.get(j);
				while((checkMe > 1) && (checkMe % p == 0))
					checkMe /= p;
			}
			if(checkMe == 1 || checkMe == -1){
				if(smoothRelFound == K+1)
					break;
				S[smoothRelFound][0] = candidates[i][0];
				S[smoothRelFound][1] = candidates[i][1];
				S[smoothRelFound][2] = a;
				S[smoothRelFound][3] = b;
				smoothRelFound++;
				//need to break if S is full
			}
		} //end trial div outer for

	/* Print output for each sieving round - testing */

			System.out.println("  x --------- g_a,b(x) --- a " + "--- b ");
		for(int j = 0; j < S.length; j++)
			System.out.printf(" %-12d %-12d %-5d %d \n", S[j][0], S[j][1], S[j][2], S[j][3]);

		polys++;										// Increment number of polynomials sieved
	}//end outer while



		
		return S;

	}//end get smooth relations 


	/**
	* This method is the same as above but calculates the smoothness bound 
	* B based on the heuristic arguments presented in Prime Numbers.
	* 
	* @param N	The integer to be factored. Must be an odd composite
	* free of prime powers.   
	*
	*/
//	public static int[] getSmoothRelations(int N){}


	// Helper Methods //


	/* Determine the factor base: the
	 * smooth primes such that N is a 
	 * quadratic residue (mod p). I.e. Legendre symbol
	 * (N/p) = 1
	 *
	 * @param F  the smoothness bound
	 * @param N  the composite begin factored
	 * @return  a list containing the elements of the 
	 * factor base. 
	 */
	private static ArrayList<Integer> determineFactorBase(int F, int N) {
		// First get primes <= F into an array
		int[] primes = SieveOfEratosthenes.basicEratos(F);  
		ArrayList<Integer> factorBase = new ArrayList<Integer>();
		// Next determine which ones give leg (N/p)=1
		// by calling DetermineQuadRes
		for(int i = 0; i < primes.length; i++) {
			int p = primes[i];
			if(DetermineQuadRes.LegJacSym(N, p) == 1)
				factorBase.add(p);
		}
		

		return factorBase;
	}

	public static void main(String[] args){
		getSmoothRelations(101868649, 233);	
	}//end main

}//end BasicQS

