import java.lang.Math.*;
import java.util.*;
import java.math.BigInteger;

/** This is an implementation of the Sieving portion of the improvement to the
Self Initializing Quadratic Sieve, published by T. Klelinjung (2016).
* Built on code written in authours SIQS implementation and based on the pseduocode provided in 
* the 'Quadratic Sieving' by Thorsten Kleinjung (2016). Specifically this code implements the 
* simplified version for M > n, as described in the referenced publication.
*
* Note: this versions does not use appropriate sized input N, as int is used here. Thus running
* time will be higher than in SIQS. 
* TODO: convert this class to a long & BigInteger version.
*
* @author Shane Sims <shane.sims.ss@gmail.com>
* @version 21 July 2017 
*
*/

public class KSIQS {


	/**
	* This method determine the smooth relations required to factor N.
	* 
	* @param N	The integer to be factored. Must be an odd composite
	* free of prime powers.   
	* @param F	The smoothness bound to which the returned relations will
	* be bound by.
	* @return 	An array with each element containing an array with length 2, 
	* representing an ordered pair of the form (x, (ax+b)^2 - N), where the value 
	* x leads (ax + b)^2-n to be B-smooth.
	*
	*/
	public static int[][] getSmoothRelations(int N, int F){
		int M;										// Term in bound [-M,M]
		ArrayList<Integer> factorBase;							// Primes which will be used in the sieving stage. 
		int[] t_p;									// Will hold t in t^2 \equiv N (mod p) for each p in Factor Base
		int[] log_p;									// Approximate logarithm for each prime in the factor base
		

		/* 1. Compute Startup Data */
		M = F/2;									// Smaller M than MPQS TODO: experiment with size of M
		factorBase = determineFactorBase(F, N);						// Initialize factorBase.


		// For all p in FB, compute and store sqrts of N (mod p)
		for(int i = 1; i < modSqrts.length; i++) {					// Solve t in t^2 \equiv N (mod p) for each p in factorBase
			t_p[i] = TonelliSqrtModP.SqrtModP(N, factorBase.get(i));
		}
		
		// For all p in FB, compute and store approximate log of p
		for(int i = 0; i < logp.length; i++) {
			log_p[i] = (int) Math.round(Math.log(factorBase.get(i))/Math.log(2));	// Get approximate log_2(p) using change of base for each factor base prime
		}



		/* 2. Initialization (Improved) */





		/* 3. Sieving Stage */



		/* 4. Trial Division Stage */



	} // end getSmoothRelations()









	//Driver for testing and running class
	public static void main(String[] args){


	}// end main

} // end KSIQS	

